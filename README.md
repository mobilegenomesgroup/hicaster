# HiCaster

## Motivation
The motivation for developing HiCaster is to create the first component of an unified framework for nucleome cell analysis. With ‘nuclelome’ we want to take in consideration the various ‘omic’ technologies that produce data representing the cell state in terms of 3D structure (Hi-C), gene expression (RNA-Seq), methylation, acetylation, etc.. In order to design such unified framework we will resort to Artificial Intelligence Autoencoders to build the model core. In essence autoencoders are neural networks that take as input an N-dimensional object (i.e a vector frequencies, image or volume), reduce it to a small representation ‘embedding’ suitable for practically any calculation (i.e subtractions, distances) and then decode it back to its original nature (i.e. back to an image shape). Even more, autoencoders can be wrapped in to a Long Short Term Memory (LSTM) allowing to study the evolution of nucleome as if it was a time series.

### Isn't this already been done?
Briefly HiCaster is a tool that allows to compare two contact maps and display the regions showing significant areas of divergence. This raises the obvious question: Isn’t there applications that already offer that capability? Yes, they exist, [CHESS](https://pubmed.ncbi.nlm.nih.gov/33077914/) to mention an excellent choice. However, as explained above we aim to integrate different ‘omic’ sources of information, which unlike Hi-C cannot be analyzed using computer vision. Hence, we need a framework that allows for a unified low dimensional representation of the multiple sources of biological data representing the cell state. Still due to the HiCaster desgin we can, and actually we do, use conventional computer vision techniques for pre and post-porcessing purposes.

In the following schema we represent how multiple autoencoders, each of them addressing a particular ‘omic’ technology, can be encoded in their corresponding embeddings which then are further conceptualized in a global or ‘nucleome’ emdedding. On the leftmost side of the schema is HiCaster, our prototype model for Hi-C differencial analysis and the subject for the rest of code, documentation and reference files in this repository.

![nucleome_framework](misc/nucleome_framework.png)

Figures are for illustration purposes only and original references can be found [here](https://pubmed.ncbi.nlm.nih.gov/24098335/), [here](https://pubmed.ncbi.nlm.nih.gov/22028803/) and [there](https://pubmed.ncbi.nlm.nih.gov/31500663/)

## Algorithm description:
### Autoencoder
Now that we have justified the development of our own Hi-C differential analysis tool we describe its internals. At the core of HiCaster there is and 2D-CNN autoencoder. Briefly there are four ReLU ResNet blocks before and after a two hundred sized embedding. Input images are 100X100 pixels, the final activation sigmoid layer renders the reconstructed input image. Once the autoencoder is trained its two components namely, the encoder and decoder are used separately. The encoder is used to represent two Hi-C contact maps as two vectors (embeddings) which are then subtracted one from another. The resulting subtracted embedding is used as the input of the decoder which produces a ‘Hi-C differential contact map’ between the two contact maps compared.

There is a note to make on how the every pair of images representing the HiC contact maps to be compared are preprocessed before entering the encoder. This preprocessing involves four sequential steps:

1.Balancing: Each input matrix is produced from a pre-existing balanced [cool file](https://cooler.readthedocs.io/en/latest/cli.html#cooler-balance) which has been balance to control for CNV bias.

2.[Histogram matching](https://scikit-image.org/docs/dev/auto_examples/color_exposure/plot_histogram_matching.html) so one of the two images compared serves as the reference to equalize differences of i.e. contrast and ligting. We believe this method can dissipate global coverage differences between two images compared.

3.[observed/expected (obs/exp) transformation](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2858594/) to remove the distance dependency of pairwise contact probabilities

4.normalization: Each images are normalized between zero and one as a standard procedure for convergence in 2D-CNNs. Once the decoder outputs the ‘Hi-C differential contact map’ we also normalize again from zero to one. This step might be unnecessary as the final activation layer of the decoder is already sigmoid.

The post-processing of the ‘Hi-C differential contact map’ involves to sequential steps:

1.normalization: This step might be unnecessary as the final activation layer of the decoder is already sigmoid.

2.[gaussian filter](https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.gaussian_filter.html): We apply a gaussian filter to produce a smoothed version of the Hi-C differential contact map more suitable for the latter segmentation step.

The following figure shows the sequential pre-preprocessing steps, starting from the balanced contacts maps of two images to be compared. In this particular example Histogram matching has little effect but that is not necessarily the case. Also note from the colour map bar that all images are normalized from zero to one.

![preprocessing](misc/preprocessing.png)



This following schema the autoencoder design (left) and the differencial Hi-C procedure (right).
![autoencoder](misc/autoencoder.png)

### Contouring
Once we have obtained the smoothed Hi-C differential contact map we segment it to isolate the regions where with significant change. We have found that applying [contour levels](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.contour.html#matplotlib.pyplot.contour) on the contact map is a simple and yet effective way of isolating level of gradual change. We use four contour levels namely high, medium-high, medium-low and low. 

For each smoothed Hi-C differential contact map we record the average intensity on each contor level. That will allow us to establish cut-offs and hence determine wether a particular contour level exhibits a statistically significant change in a given Hi-C differential contact map.

We also map each contour to genome coordinates using their minimum and maximum values on X and Y axis.

The following figure illustrates the contouring process in a particular genomic region being compared for two cell lines. Note that because smoothed Hi-C differential contact maps are symmetric (or nearly symmetric) there is redundancy in the coordinates being displayed. 

![contouring](misc/contouring.png)

As explained above we record the average change intensity on each contour level for every Hi-C differential contact map generated. For example, two human full genome Hi-C contact maps generate over seven hundred pairwise 100x100 image comparisons of 2.5Gb at 25Kb resolution. In our setting each of those comparisons generates a Hi-C differential contact map, which in turn renders four contour levels with their corresponding four average change intensities. We therefore create four frequency histograms of average change intensities, one for each contour level, as our background model to establish significance cut-offs. 

Each histogram shows a main peak plus other minor peaks. We attribute the minor peaks to a cross-contamination of observations among contour levels, indicating that we should better optimize the number of contour levels, possibly increasing them. Nevertheless, the main peak for each level is evident and cross-contamination peaks can be post-reassigned in code. After such correction we have tested for [normality](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.normaltest.html) on each of the four levels. Normality was found in the 'high' and 'medium-high' levels. On the 'medium-low' level we had to resort to logarithmic transformation and and in the 'low' level normality was clearly not hold. Nevertheless, as each of the four levels is a separate hyphotesis we can calulate normal distribution p-values in the 'high', 'medium-high' and 'medium-low' levels while using [wilcoxon](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.wilcoxon.html) test for the 'low' level.

In the figure below we show the four histograms of intensity change frequencies, the cross-level contamination and the reassigning fix. We also show the results of the normality test for each level.

![histograms](misc/histograms.png)

Next figure shows an example of finding statistically significant p-values for each level on a Hi-C differential contact map. In this particular example only the two regions within the 'high' levels are found relevant. However p-values on each contour level are venetuallt corrected to account for [false discovery rate](https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.SelectFdr.html).

![histograms](misc/pvalues.png)

## Training/Test dataset
At its very heart HiCaster requires of an autoencoder to be trained/tested. We provide a training dataset available [here](reference_files/GSM1551603_HIC054_30_H2009_H2087_25000.npz):

Such file contains 4528 100x100x1 arrays. Each of those arrays resprent 2.5Mb x 2.5Mb contact maps at 25kb resolution, that's why the arrays are 100x100 (2.5Mb/25Kb = 100). The actual contact maps come from the Hi-C experiments for these four cell lines. Note that regions with no contacts (i.e. centromeres were removed). 
### From [Rao et al. 2014](https://pubmed.ncbi.nlm.nih.gov/25497547/):
-  [IMR90 (CCL-186)](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM1551603)
[link to the .hic file](https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM1551603&format=file&file=GSM1551603%5FHIC054%5F30%2Ehic)
We transformed that .hic file into this  balanced [.cool](reference_files/GSM1551603_HIC054_30_25000.cool) file of 2.5Kb resolution

- [GM12878](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM1551688)
[link to the .hic file](https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE63525&format=file&file=GSE63525%5FGM12878%5Finsitu%5Fprimary%2Breplicate%5Fcombined%5F30%2Ehic)
We transformed that .hic file into this  balanced [.cool](reference_files/GSE63525_GM12878_insitu_primary+replicate_combined_30.25Kb.cool) file of 2.5Kb resolution.

### From our own experiments:
- [H2087](https://web.expasy.org/cellosaurus/CVCL_1524)
[link to the 2.5Kb .cool file](reference_files/H2087_25Kb.cool)
- [H2009](https://web.expasy.org/cellosaurus/CVCL_1514)
[link to the 2.5Kb .cool file](reference_files/H2009_25Kb.cool)

To produce the [training .npz file](eference_files/GSM1551603_HIC054_30_H2009_H2087_25000.npz) from those four .cool files you just need to run:

    python hicaster/src/create_dataset.py training \
    -i hicaster/reference_files \
    -o $OUTPUTDIR/training.npz \
    -f hicaster/reference_files/hs37d5.fa.fai

## Training code
The training code is used to train and test an autoencoder and save its two components namely, the ecoder and the decoder.

You can try the training code in [coogle colab](https://colab.research.google.com/drive/1h9QovpjOe4Pp7kIC2vFxtPKwnCMIrZbS?usp=sharing) or run that same [code](src/hicaster_train.py) on your own environment.

    python hicaster_train.py GSM1551603_HIC054_30_H2009_H2087_25000.npz $OUTDIR

## Example dataset
The actual data you would analyse with HiCaster are pairs of contact maps. Each pair is accompained with its chr:start-end.
We provide two .npz files from our two Hi-C experiments on cell lines. Each one contains 713 100x100x1 arrays.
Each of those arrays resprent 2.5Mb x 2.5Mb contact maps at 25kb resolution.
Also the coords.bin contains a list of the chr:start-end for each contact map.
These are the links to the actual files:

 [H2009.npz]('reference_files/H2009.npz')
 
 [H2087.npz]('reference_files/H2087.npz') 
 
 [coords.bin]('eference_files/coords.bin)


To reproduce the [H2009.npz](reference_files/H2009.npz), [H2087.npz](reference_files/H2087.npz) and [coords.bin](reference_files/coords.bin) you need to run:

    python hicaster/src/create_dataset.py comparison \
    -a hicaster/reference_files/H2009_25Kb.cool \
    -b hicaster/reference_files/H2087_25Kb.cool \
    -d $OUTPUTDIR \
    -f hicaster/reference_files/hs37d5.fa.fai



## Analysis code
To run HiCaster with the example dataset you could try the analysis code in [coogle colab](https://colab.research.google.com/drive/1Y6OKr4WkPbX__TI94hLi6OCO7ESVjXai?usp=sharing) or run that same code [hicaster_analyse.py](src/hicaster_analyse.py) on your own environment.

    python hicaster/src/hicaster_analyse.py \
    hicaster/models/encoder.sigmoid.h5 \
    hicaster/models/decoder.sigmoid.h5 \
    hicaster/reference_files/all_average_intensities.npz \
    hicaster/reference_files/H2009.npz \
    hicaster/reference_files/H2087.npz

That will take about two hours to run and will produce three .png files per pair of images compared and per level with pvalue <= 0.05:
1. 'a' sample normalize 
2. 'b' sample normalize
3. 'a-b' differencial contact map
There will be a report.txt file showing:
level [index, chr, start, end, [regions], pvalue, pvalue_corrected, to_keep")

This [images.tar.gz](results/images.tar.gz) contains the result images and [report.txt](results/report.txt) for the example dataset.


## Further work
We are currenlty investigating to properties two the model:
- the 'medium-low' level seems to be highligting differential areas that resemble loops. In the figure below:
Left: peak calling, taken from Fig 3 [Rao et al. 2014](https://pubmed.ncbi.nlm.nih.gov/25497547/)
Right: two HiCaster differencial contact maps highligting 'medium-low' differencial feautures. 
![peak_calling](misc/peak_calling.png)

- the ResNet block activations maps of the autoencoder can exploited to potentially boost the saliency of individual biological features. This property of CNNs has been already demonstrated in other [fields](https://arxiv.org/pdf/2005.07623.pdf). Our preliminary analysis also show that  the internal activation maps such 'f${NUMBER}.sigmoid.h5' in the [moldels folder](models) can be pontentially used directly to isolate differential levels, as in the example below.
![internal_activation_maps](misc/internal_activation_maps.png)
