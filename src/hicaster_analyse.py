# Commented out IPython magic to ensure Python compatibility.
# %matplotlib inline
import matplotlib.pyplot as plt
import matplotlib
import keras
from keras import layers
from keras.initializers import RandomNormal
from PIL import Image
import numpy as np
from numpy import asarray, max, min, copy
import glob
from numpy import empty, savez_compressed, load
from scipy.spatial.distance import pdist
from scipy.cluster.hierarchy import dendrogram, linkage
import plotly.express as px
import plotly.graph_objects as go
from IPython.display import HTML
import pandas as pd
import plotly.graph_objects as go
from collections import defaultdict
import warnings
import tensorflow as tf
import cv2
from google.colab.patches import cv2_imshow
import scipy.misc
import matplotlib.pyplot as plt
from keras.initializers import RandomNormal
from keras.models import Model
from keras.models import Input
from keras.layers import Conv2D
from keras.layers import Conv2DTranspose
from keras.layers import LeakyReLU
from keras.layers import Activation
from keras.layers import Concatenate
from keras.layers import Dropout
from keras.layers import BatchNormalization
from keras.layers import LeakyReLU
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import add
from keras.layers import Reshape
from keras.utils.vis_utils import plot_model
from keras import backend as K
from keras.optimizers import Adam
from tensorflow.keras import layers
import keras
import PIL
import warnings
import re
import matplotlib.patches as patches
import scipy.ndimage as ndimage
from skimage import exposure
from skimage.exposure import match_histograms
import pickle
from copy import deepcopy
from scipy import stats
from scipy.stats import norm, wilcoxon
from sklearn.preprocessing import PowerTransformer
from statsmodels.stats.multitest import fdrcorrection, multipletests

def plot_chr(decoded,side_length,cmap,outfile=None,polygons=[]):
    fig, ax = plt.subplots(1)
    pos = ax.imshow(decoded.reshape(side_length, side_length), cmap=cmap)
    for polygon in polygons:
      ax.add_patch(polygon)
    fig.colorbar(pos, ax=ax) 
    if outfile:
      plt.savefig(outfile)
    else:
      plt.show()
    plt.close()

"""histogram function"""

def hist(points):
  plt.hist(points, bins='auto')  # arguments are passed to np.histogram
  plt.title("Histogram with 'auto' bins")
  #Text(0.5, 1.0, "Histogram with 'auto' bins")
  plt.show()

def exlude_by_intensity(points,bw,intensity):
  points_filtered = list()
  for i in range(points.shape[0]):
    if points[i,2] >= intensity:
      points_filtered.append(points[i])
    else:
      #print(points[i,0])
      bw[ int(points[i,0]) , int(points[i,1]) ] = 0
  return asarray(points_filtered),bw

"""helper function for obs/exp normalization.

returns a dictionary mapping distance to average intensity
"""

def get_expected_intensity(in_image):
  image = np.copy(in_image)
  distance2intensity = defaultdict(lambda: 0.0)
  distance2count= defaultdict(lambda: 0) 
  for i in range(0,image.shape[0]):
    for j in range(0,image.shape[1]):
      
      distance = int(abs(i-j))
      intensity = image[i,j,0]
      distance2intensity[distance]+=intensity
      distance2count[distance]+=1
      #if (i == 20 and j == 80) or (i == 80 and j == 20):
      #  print(i,j,distance,intensity)
  for distance, intensity in distance2intensity.items():
    #print(distance, intensity,distance2count[distance])
    distance2intensity[distance]=intensity/distance2count[distance]
  return distance2intensity

"""function that normalizes the contact map based on the average intensity expected a particular genomic distance"""

def normalize_obs_exp(in_image,distance2intensity):
  image = np.copy(in_image)
  for i in range(0,image.shape[0]):
    for j in range(0,image.shape[1]):
      distance = int(abs(i-j))
      try:
        image[i,j]=image[i,j]/distance2intensity[distance]
      except:
        print(i,j,distance,distance2intensity[distance])
  return image

"""function to normalize the contact map from 0 to 1.

this is a 'requirement' for the autoencoder to 
work.
"""

def normalize_0_1(in_image):
  image = np.copy(in_image)
  if min(image) >= 0:
    image-=np.min(image)
  else:
    image+=abs(np.min(image))
  image = image/np.max(image)
  return image

def normalize(images,side_length):
  input_size = side_length * side_length

  images_normalized=list()

  for index,image in enumerate(images):
    distance2intensity=get_expected_intensity(image)
    image = normalize_obs_exp(image,distance2intensity)
    image=normalize_0_1(image)
    images_normalized.append(image)

    
  images_normalized=np.asarray(images_normalized)
  images_normalized=images_normalized.reshape((-1,side_length,side_length,1))

  return images_normalized

"""download the trained encoder and decoder and precalculated average change intensities"""

encoder = keras.models.load_model('encoder.sigmoid.h5',compile=False)
decoder = keras.models.load_model('decoder.sigmoid.h5',compile=False)
intensities=load('all_average_intensities.npz')

def binarize(array):
  binarized = cv2.GaussianBlur(array,(5,5),0)
  _,binarized = cv2.threshold(array,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
  return binarized

def contour_to_rect(contour,start,end,side_length, colour='y'):
  x_min,x_max = int(min(contour[:,0])), int(max(contour[:,0]))
  y_min,y_max = int(min(contour[:,1])), int(max(contour[:,1]))
  
  rect = patches.Rectangle((x_min,y_min),abs(x_min - x_max),abs(y_min - y_max),edgecolor=colour , facecolor="none", lw = 2)
  
  return rect

def plot_frame_to_file(frame,out_file):
  plt.subplot(1, 1, 1)
  plt.imshow(frame,cmap="inferno_r")
  plt.axis('off')
  plt.savefig(out_file,bbox_inches='tight',transparent=False, pad_inches=0, dpi=600)

def contour_to_coordinates(contour,chr,start,end,side_length):
  x_min,x_max = int(min(contour[:,0])), int(max(contour[:,0]))
  y_min,y_max = int(min(contour[:,1])), int(max(contour[:,1]))
  

  x_start_diff_TAD =  int(abs(start - end) * (x_min -0) / (side_length - 0))
  x_end_diff_TAD =  int(abs(start - end) * (x_max -0) / (side_length - 0))


  y_start_diff_TAD =  int(abs(start - end) * (y_min -0) / (side_length - 0))
  y_end_diff_TAD =  int(abs(start - end) * (y_max -0) / (side_length - 0))

  #print(chr,x_start_diff_TAD,x_end_diff_TAD ,y_start_diff_TAD,y_end_diff_TAD)
  return (chr,x_start_diff_TAD,x_end_diff_TAD ,y_start_diff_TAD,y_end_diff_TAD)

def get_average_intensities(image,side_length, levels=3):

  x, y = np.meshgrid(range(0,side_length),range(0,side_length))
  fig , ax = plt.subplots()
  ax.invert_yaxis()
  contours = ax.contour(x,y,image,levels)

  contour2intensities = defaultdict(lambda: list())
  for i in range(side_length):
    for j in range(side_length):
      contour = contours.find_nearest_contour(i, j, indices=None, pixel=False)[0]
      contour2intensities[ contour ].append(image[i,j])

  contour2average_intensity = defaultdict(lambda: 0)
  
  for k in contour2intensities.keys():
    contour2average_intensity[k]= np.mean(contour2intensities[k])
  
  means = list(contour2average_intensity.values())
  #print(means)
  return ({'high': means[0], 'medium_high': means[1], 'medium_low': means[2], 'low': means[3]}, contours)

def get_rectangles(contours,chr,start,end,side_length):
  
  number_of_levels = len(contours.allsegs)
  colours = list(matplotlib.colors.cnames.values())                    
  all_polygons=list()
  all_coords=list()
  for i,this_level_contours in enumerate(contours.allsegs): 
      this_level_polygons=list()
      this_level_coords=list()
      colour = colours[i*6]
      for contour in this_level_contours:
        rect =  contour_to_rect(contour,start,end,side_length, colour)
        coords = contour_to_coordinates(contour,chr,start,end,side_length)     
       
        this_level_polygons.append(rect)
        this_level_coords.append(coords)
      #print(len(this_level_poligons))
      
      all_polygons.append(this_level_polygons)
      all_coords.append(this_level_coords)
  #print('-------------')
  return list(reversed(all_polygons[:-1])) + list(reversed(all_coords[:-1]))    # last level is nothing I believe, and then I need to reverse to go from high to low

def get_reassign_intensities(intensities):
	high=intensities['high_all_average_intensities'][intensities['high_all_average_intensities'] > 0.6]
	add_medium_high=intensities['medium_high_all_average_intensities'][intensities['medium_high_all_average_intensities'] > 0.6]
	add_medium_low=intensities['medium_low_all_average_intensities'][intensities['medium_low_all_average_intensities'] > 0.6]
	add_low=intensities['low_all_average_intensities'][intensities['low_all_average_intensities'] > 0.6]
	all_high=np.hstack([high,add_medium_high,add_medium_low,add_low])

	medium_high = intensities['medium_high_all_average_intensities'] [ (intensities['medium_high_all_average_intensities'] > 0.4) & (intensities['medium_high_all_average_intensities'] < 0.6 )]
	add_high = intensities['high_all_average_intensities'] [ (intensities['high_all_average_intensities'] > 0.4) & (intensities['high_all_average_intensities'] < 0.6 )]
	all_medium_high=np.hstack([medium_high,add_high])

	medium_low = intensities['medium_low_all_average_intensities'] [ (intensities['medium_low_all_average_intensities'] > 0.2) & (intensities['medium_low_all_average_intensities'] < 0.4 )]
	add_high = intensities['high_all_average_intensities'] [ (intensities['high_all_average_intensities'] > 0.2) & (intensities['high_all_average_intensities'] < 0.4 )]
	add_medium_high = intensities['medium_high_all_average_intensities'] [ (intensities['medium_high_all_average_intensities'] > 0.2) & (intensities['medium_high_all_average_intensities'] < 0.4 )]
	add_low = intensities['low_all_average_intensities'] [ (intensities['low_all_average_intensities'] > 0.2) & (intensities['low_all_average_intensities'] < 0.4 )]
	all_medium_low = np.hstack([medium_low,add_high,add_medium_high,add_low])
	all_medium_low = np.log2(all_medium_low)
	
	low = intensities['low_all_average_intensities'] [intensities['low_all_average_intensities'] < 0.3 ]
	add_medium_low = intensities['medium_low_all_average_intensities'] [ (intensities['medium_low_all_average_intensities'] < 0.1) ]
	all_low = np.hstack([low,add_medium_low])
	
	return(all_high,all_medium_high,all_medium_low,all_low)

def get_pvalues(high, medium_high, medium_low, low, all_high, all_medium_high, all_medium_low, all_low ):
  pvalue_high = 1-norm.cdf(high,np.mean(all_high),np.std(all_high))
  pvalue_medium_high = 1-norm.cdf(medium_high,np.mean(all_medium_high),np.std(all_medium_high))
  pvalue_medium_low= 1-norm.cdf(np.log2(medium_low),np.mean(all_medium_low),np.std(all_medium_low))
  _,pvalue_low=wilcoxon(all_low - low, zero_method='wilcox', correction=False)
  if (pvalue_low < 0.05) & (low < np.median(all_low)):
    pvalue_low = 0.5
  return pvalue_high,pvalue_medium_high, pvalue_medium_low, pvalue_low

def collect_stats(all_average_intensities,high,medium_high,medium_low,low):
    all_average_intensities['high'].append(high)
    all_average_intensities['medium_high'].append(medium_high)
    all_average_intensities['medium_low'].append(medium_low)
    all_average_intensities['low'].append(low)

def correct_p_values(pvalues_high,pvalues_medium_high,pvalues_medium_low,pvalues_low):
  pvalues_high_corrected = multipletests(pvals=pvalues_high, alpha=0.05, method="fdr_bh")
  pvalues_medium_high_corrected = multipletests(pvals=pvalues_medium_high, alpha=0.05, method="fdr_bh")
  pvalues_medium_low_corrected = multipletests(pvals=pvalues_medium_low, alpha=0.05, method="fdr_bh")
  pvalues_low_corrected = multipletests(pvals=pvalues_low, alpha=0.05, method="fdr_bh")
  return pvalues_high_corrected, pvalues_medium_high_corrected, pvalues_medium_low_corrected, pvalues_low_corrected

def add_corrected_pvalues(summary,pvalues_high_corrected, pvalues_medium_high_corrected, pvalues_medium_low_corrected, pvalues_low_corrected):
  for index,record in enumerate(summary['high']):
    record.append(pvalues_high_corrected[1][index])
    record.append(pvalues_high_corrected[0][index])
  for index,record in enumerate(summary['medium_high']):
    record.append(pvalues_medium_high_corrected[1][index])
    record.append(pvalues_medium_high_corrected[0][index])
  for index,record in enumerate(summary['medium_low']):
    record.append(pvalues_medium_low_corrected[1][index])
    record.append(pvalues_medium_low_corrected[0][index])
  for index,record in enumerate(summary['low']):
    record.append(pvalues_low_corrected[1][index])
    record.append(pvalues_low_corrected[0][index])
  return summary

"""load dataset"""

if __name__ == '__main__':

encoder_file_h5=sys.argv[1]
decoder_file_h5=sys.argv[2]
average_intensities_file = sys.argv[3]
sample_a_file_npz = sys.argv[4]
sample_b_file_npz = sys.argv[5]
coords_file = sys.argv[6]

encoder = keras.models.load_model(encoder_file_h5,compile=False)
decoder = keras.models.load_model(decoder_file_h5,compile=False)
intensities=load(average_intensities_file')
  
  side_length=100
  A = load(sample_a_file_npz)
  A = A['hic']
  A = A.reshape(-1,side_length, side_length, 1)
  A+=0.000001
  A_normalized=normalize(A,side_length)
  #print(A_normalized.shape)
  B = load(sample_b_file_npz)
  B = B['hic']
  B = B.reshape(-1,side_length, side_length, 1)
  B+=0.000001
  B_normalized=normalize(B,side_length)
  #print(B_normalized.shape)
  input_file = open(coords_file, "rb")
  coords_list = pickle.load(input_file)
  #print(len(coords_list))
  
  all_high,all_medium_high,all_medium_low,all_low=get_reassign_intensities(intensities)
  
  levels=3
  cmap = 'gist_stern'
  summary={'high':[], 'medium_high':[], 'medium_low':[], 'low':[] }
  all_average_intensities={'high':[], 'medium_high':[], 'medium_low':[], 'low':[] }
  
  pvalues_high = list()
  pvalues_medium_high = list()
  pvalues_medium_low = list()
  pvalues_low = list()
  
  for i in range(A.shape[0]):
    index = i
    print(index)
  #for index,i in enumerate([18,52,56,502,559]):
    a = A[i:i+1]
    b_ = B[i:i+1]
    b = match_histograms(b_, a, multichannel=False)
    c = normalize(a,side_length)
    d = normalize(b,side_length)
    e = decoder.predict(encoder.predict(c))
    f = decoder.predict(encoder.predict(d))
    g = decoder.predict( encoder.predict(c) - encoder.predict(d))
    g_ = decoder.predict( encoder.predict(a) - encoder.predict(b))
  
    images_list = [a,b_,b,c,d,e,f,g,g_] 
    images_list = list(map( lambda image: np.reshape(image,(100,100)) ,images_list))
    images_list = list(map( lambda image: (image*255).astype(np.uint8), images_list))
    images_smoothed_list = list(map(lambda array : ndimage.gaussian_filter(array, sigma=(5), order=0), images_list))
  
    ret = re.match(r'^(.*?):(.*?)-(.*?)$',coords_list[i])
    chr = ret.group(1)
    start = int(ret.group(2))
    end = int(ret.group(3))  
    
  
    
    average_intensities, contours = get_average_intensities(normalize_0_1(images_smoothed_list[7]),side_length,levels) 
    high=average_intensities['high']
    medium_high=average_intensities['medium_high']
    medium_low=average_intensities['medium_low']
    low=average_intensities['low']
    
    # this is only needed if you want to get new distributions
    #collect_stats(all_average_intensities,high,medium_high,medium_low,low)
  
    pvalue_high,pvalue_medium_high, pvalue_medium_low, pvalue_low = get_pvalues(high, medium_high, medium_low, low, all_high, all_medium_high, all_medium_low, all_low )
    
    pvalues_high.append(pvalue_high)
    pvalues_medium_high.append(pvalue_medium_high)
    pvalues_medium_low.append(pvalue_medium_low)
    pvalues_low.append(pvalue_low)
  
    
    polygons_high,polygons_medium_high,polygons_medium_low,polygons_low,coords_h,coords_mh,coords_ml,coords_l = get_rectangles(contours,chr,start,end,side_length)
    summary['high'].append([index, chr, start, end, str(coords_h),pvalue_high])
    if pvalue_high <= 0.05:
      file_name="{}__{}__{}__{}__{}_{}_{}.png".format('a_minus_b_equalized','normalized','high',index,chr,start,end)
      plot_chr(normalize_0_1(images_smoothed_list[7]),side_length,cmap,file_name,polygons_high)
      
      polygons_high,polygons_medium_high,polygons_medium_low,polygons_low,coords_h,coords_mh,coords_ml,coords_l = get_rectangles(contours,chr,start,end,side_length)
      file_name="{}__{}__{}__{}__{}_{}_{}.png".format('a','normalized','high',index,chr,start,end)
      plot_chr(c,side_length,cmap, file_name,polygons_high)
      
      polygons_high,polygons_medium_high,polygons_medium_low,polygons_low,coords_h,coords_mh,coords_ml,coords_l = get_rectangles(contours,chr,start,end,side_length)
      file_name="{}__{}__{}__{}__{}_{}_{}.png".format('b','normalized','high',index,chr,start,end)
      plot_chr(d,side_length,cmap,file_name,polygons_high)
  
    polygons_high,polygons_medium_high,polygons_medium_low,polygons_low,coords_h,coords_mh,coords_ml,coords_l = get_rectangles(contours,chr,start,end,side_length)
    summary['medium_high'].append([index, chr, start, end, str(coords_mh),pvalue_medium_high])
    if pvalue_medium_high <= 0.05:
      file_name="{}__{}__{}__{}__{}_{}_{}.png".format('a_minus_b_equalized','normalized','medium_high',index,chr,start,end)
      plot_chr(normalize_0_1(images_smoothed_list[7]),side_length,cmap,file_name,polygons_medium_high)
      
      polygons_high,polygons_medium_high,polygons_medium_low,polygons_low,coords_h,coords_mh,coords_ml,coords_l = get_rectangles(contours,chr,start,end,side_length)
      file_name="{}__{}__{}__{}__{}_{}_{}.png".format('a','normalized','medium_high',index,chr,start,end)
      plot_chr(c,side_length,cmap, file_name,polygons_medium_high)
      
      polygons_high,polygons_medium_high,polygons_medium_low,polygons_low,coords_h,coords_mh,coords_ml,coords_l = get_rectangles(contours,chr,start,end,side_length)
      file_name="{}__{}__{}__{}__{}_{}_{}.png".format('b','normalized','medium_high',index,chr,start,end)
      plot_chr(d,side_length,cmap,file_name,polygons_medium_high)
  
    polygons_high,polygons_medium_high,polygons_medium_low,polygons_low,coords_h,coords_mh,coords_ml,coords_l = get_rectangles(contours,chr,start,end,side_length)
    summary['medium_low'].append([index, chr, start, end, str(coords_ml),pvalue_medium_low])
    if pvalue_medium_low <= 0.05:
      file_name="{}__{}__{}__{}__{}_{}_{}.png".format('a_minus_b_equalized','normalized','medium_low',index,chr,start,end)
      plot_chr(normalize_0_1(images_smoothed_list[7]),side_length,cmap,file_name,polygons_medium_low)
      
      polygons_high,polygons_medium_high,polygons_medium_low,polygons_low,coords_h,coords_mh,coords_ml,coords_l = get_rectangles(contours,chr,start,end,side_length)
      file_name="{}__{}__{}__{}__{}_{}_{}.png".format('a','normalized','medium_low',index,chr,start,end)
      plot_chr(c,side_length,cmap, file_name,polygons_medium_low)
      
      polygons_high,polygons_medium_high,polygons_medium_low,polygons_low,coords_h,coords_mh,coords_ml,coords_l = get_rectangles(contours,chr,start,end,side_length)
      file_name="{}__{}__{}__{}__{}_{}_{}.png".format('b','normalized','medium_low',index,chr,start,end)
      plot_chr(d,side_length,cmap,file_name,polygons_medium_low)
  
  
    polygons_high,polygons_medium_high,polygons_medium_low,polygons_low,coords_h,coords_mh,coords_ml,coords_l = get_rectangles(contours,chr,start,end,side_length)
    summary['low'].append([index, chr, start, end, str(coords_l),pvalue_low])
    if pvalue_low <= 0.05:
      file_name="{}__{}__{}__{}__{}_{}_{}.png".format('a_minus_b_equalized','normalized','low',index,chr,start,end)
      plot_chr(normalize_0_1(images_smoothed_list[7]),side_length,cmap,file_name,polygons_low)
      
      polygons_high,polygons_medium_high,polygons_medium_low,polygons_low,coords_h,coords_mh,coords_ml,coords_l = get_rectangles(contours,chr,start,end,side_length)
      file_name="{}__{}__{}__{}__{}_{}_{}.png".format('a','normalized','low',index,chr,start,end)
      plot_chr(c,side_length,cmap, file_name,polygons_low)
      
      polygons_high,polygons_medium_high,polygons_medium_low,polygons_low,coords_h,coords_mh,coords_ml,coords_l = get_rectangles(contours,chr,start,end,side_length)
      file_name="{}__{}__{}__{}__{}_{}_{}.png".format('b','normalized','low',index,chr,start,end)
      plot_chr(d,side_length,cmap,file_name,polygons_low)
  
  
  
  pvalues_high_corrected, pvalues_medium_high_corrected, pvalues_medium_low_corrected, pvalues_low_corrected = correct_p_values(pvalues_high,pvalues_medium_high,pvalues_medium_low,pvalues_low) 
  summary = add_corrected_pvalues(summary,pvalues_high_corrected, pvalues_medium_high_corrected, pvalues_medium_low_corrected, pvalues_low_corrected)
  
  
  f = open('report.txt','w')
  f.write("level [index, chr, start, end, [regions], number_of_regions, intensity, pvalue, pvalue_corrected, to_keep]")
  for level,records in summary.items():
    for record in records:
      f.write(level +' '+str(record) + '\n')
  f.close()
  
  # this is only needed if you want to get new distributions
  '''high_all_average_intensities=all_average_intensities['high']
  medium_high_all_average_intensities=all_average_intensities['medium_high']
  medium_low_all_average_intensities=all_average_intensities['medium_low']                                                                                        
  low_all_average_intensities=all_average_intensities['low'] 
  savez_compressed('all_average_intensities.npz',
  high_all_average_intensities=high_all_average_intensities,
  medium_high_all_average_intensities=medium_high_all_average_intensities,
  medium_low_all_average_intensities=medium_low_all_average_intensities,
  low_all_average_intensities=low_all_average_intensities)'''
