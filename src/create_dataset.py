import sys
import glob
import argparse
import csv
import cooler
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from numpy import empty, savez_compressed
import os
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import array_to_img
import pickle

def get_chr_lengths(chrs,fai):
    chr2length = dict()
    with open(fai, 'r') as f:
        reader = csv.reader( f, delimiter='\t')
        for line in reader:
            chr = line[0]
            length = line[1]
            if chr in chrs:
                #print(chr)
                chr2length[chr]=int(length)
    return chr2length    

def create_grids(chr2length,resolution):
    chr2grid = dict()
    chr2linspace = dict()
    for chr,length in chr2length.items():
        n_chunks= int(chr2length[chr]/resolution)
        chunks = list(map( lambda limit: int(limit), np.linspace(0,length,n_chunks)))
        #print("{} {} {} {} _ {}".format( chr,length,chunks[0],chunks[-1],str(chunks) ))
        chr2linspace[str(chr)]=np.asarray(chunks)
    
    return chr2linspace

def normalize_0_1(in_image):
  image = np.copy(in_image)
  if np.min(image) >= 0:
    image-=np.min(image)
  else:
    image+=abs(np.min(image))
  image = image/np.max(image)
  return image

def get_dump_files(path):
    files = glob.glob(  path + '/*.txt')
    return files

def txt_to_array(file):
    hic_matrix  = np.genfromtxt(file, delimiter='\t', dtype='float64')
    return hic_matrix[:,:-1]

def array_2_image(array):
    image = Image.fromarray(np.uint8(array * 255) , 'L')
    return image

def resize_image(image, resolution):
    return image.resize((resolution,resolution))

def image_2_array(image):
    return img_to_array(image, dtype='float64')


def make_output_file_name(comparison_outdir,cool):
  base=os.path.basename(cool)
  file_ext=os.path.splitext(base)[0]
  name = os.path.splitext(base)[0]
  return os.path.join(comparison_outdir,name+'.npz')

def coords2array(cb,coords):
  matb = cb.matrix(balance=True).fetch(coords)
  arrayb = normalize_0_1(matb)
  imageb = array_2_image(arrayb)
  imageb = resize_image(imageb, resolution)
  arrayb = image_2_array(imageb)
  arrayb = normalize_0_1(arrayb)
  return arrayb 

def get_user_args():
  
  # create the top-level parser
  parser = argparse.ArgumentParser(prog=sys.argv[0])
  subparsers = parser.add_subparsers(help='help !!',dest='command')
  parser_a = subparsers.add_parser('training', help='training_dataset help')
  parser_b = subparsers.add_parser('comparison', help='comparison_dataset help')
    
  parser_a.add_argument("-i", "--inputdir",  dest='inputdir', action='store', help="input dir with .cool files to create trainig dataset", required=True)
  parser_a.add_argument("-o", "--output",  dest='output', action='store', help="output .npz file containing the training dataset", required=True)
  parser_a.add_argument("-f", "--fai",  dest='fai', action='store', help="fai file", required=True)
  
  parser_b.add_argument("-a", "--coola", dest='coola', action='store', help="cool 'a' to be compared i.e. wildtype", required=True)
  parser_b.add_argument("-b", "--coolb", dest='coolb', action='store', help="cool 'b' to be compared i.e. mutant", required=True)
  parser_b.add_argument("-d", "--dir", dest='dir', action='store', help="output dir for comparison dataset", required=True)  
  parser_b.add_argument("-f", "--fai",  dest='fai', action='store', help="fai file", required=True)
  
  return parser



if __name__ == '__main__':
  parser = get_user_args()
  args = parser.parse_args()
  size=2500000
  resolution = 100
  
  if args.command == 'training':
    inputdir=args.inputdir
    fai=args.fai
    outfile=args.output

    cools = glob.glob( inputdir+'/*.cool')
    if len(cools) == 0:
      print('no .cool files found')
      exit(0)

    chrs = list(map( lambda x : str(x),  list(range(1,23))+['X','Y','MT']))
    chr2length = get_chr_lengths(chrs,fai)
    chr2linspace = create_grids(chr2length,size)
      
    dataset=list()
    for cool in cools:
      c = cooler.Cooler(cool)
      for chr,space in chr2linspace.items():
        for i,_ in enumerate(space):
          try:
            coords = "{0}:{1}-{2}".format(chr,space[i],space[i+1])
            array = coords2array(c,coords)
            if np.max(array) > 0.0:
              dataset.append(array)
    
          except Exception as e:
            print(e)
       
    np.savez_compressed(outfile,hic=dataset)
        
  elif args.command == 'comparison':
    coola =  args.coola          
    coolb =  args.coolb
    comparison_outdir = args.dir
    fai = args.fai
    
    if not os.path.exists(comparison_outdir):
      os.makedirs(comparison_outdir)
    
    ca = cooler.Cooler(coola)
    cb = cooler.Cooler(coolb)

    outfilea = make_output_file_name(comparison_outdir,coola)
    outfileb = make_output_file_name(comparison_outdir,coolb)
    outlabels = os.path.join(comparison_outdir,'coords.bin')  

    chrs = list(map( lambda x : str(x),  list(range(1,23))+['X','Y','MT']))
    chr2length = get_chr_lengths(chrs,fai)
    chr2linspace = create_grids(chr2length,size)

    
    
    dataseta=list()
    datasetb=list()
    
    out_file = open(outlabels, "wb")
    coords_container=list() 
    for chr,space in chr2linspace.items():
      for i,_ in enumerate(space):
        try:
          coords = "{0}:{1}-{2}".format(chr,space[i],space[i+1])
          arraya = coords2array(ca,coords) 
          arrayb = coords2array(cb,coords) 

          if np.max(arraya) > 0.0 and np.max(arrayb) > 0.0:
            dataseta.append(arraya)
            datasetb.append(arrayb)
            coords_container.append(coords)
        
        except Exception as e:
          print(e)
    
    np.savez_compressed(outfilea,hic=dataseta)
    np.savez_compressed(outfileb,hic=datasetb)
    pickle.dump(coords_container, out_file)
    out_file.close()
  else:
    parser.print_help()
